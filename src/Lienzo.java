import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;


public class Lienzo extends javax.swing.JPanel {

	public Lienzo() 
	{
		this.setSize(600, 600); //se selecciona el tamaño del panel
	}
 
	//Se crea un método cuyo parámetro debe ser un objeto Graphics
 
	public void paint(Graphics grafico) 
	{
		grafico.setColor(Color.WHITE); 
		grafico.fillRect(0, 0, 600, 600);
		setOpaque(false);
		super.paintComponent(grafico);
	}
	
	/**
  	pinta con degrade de un solo color  
	*/
	public void degrade()
	{
		int i;
		BufferedImage img = new BufferedImage(600, 600, BufferedImage.TYPE_INT_ARGB);	
		for (int x=0; x< img.getWidth(); x++)
			for (int y=0; y < img.getHeight(); y++)
			{
				i = 255 - (255 * (img.getWidth() - y)/img.getWidth());
				Color c = new Color (255,255-i,0);
				img.setRGB(x, y,c.getRGB());
			}
				
		this.getGraphics().drawImage(img, 0, 0, null);
	}
	
	/**
	 * Mapea el fractal en el espacio.
	 */
	public void map ()
	{
		BufferedImage img = new BufferedImage(600, 600, BufferedImage.TYPE_INT_ARGB);
		double rMax = 0.5; double rMin = -1.5; double iMax = 1.0; double iMin = -1.0;
		double rRange = (rMax - rMin)/ img.getWidth();
		double iRange = (iMax - iMin)/ img.getHeight();
		int i;
		for (int px = 0; px < img.getWidth(); px++)
			for (int py = 0; py < img.getHeight(); py++)
			{
				double zr = px * rRange + rMin;
				double zi = (img.getHeight() - py) * iRange + iMin;
				ComplexNumber z = new ComplexNumber(zr,zi);
				//pinto la imagen
				int fractal = manowar(z);
				Color c = new Color ( 255 - fractal,255 - fractal ,fractal);
				img.setRGB(px, py,c.getRGB());	
			}
		this.getGraphics().drawImage(img, 0, 0, null);
	}
	
//Metodos private
	
	// Calcular el valor para pintar un fractal	
	/**
  	Fractal de Mandelbrot: Zri = Zri-1(2) + Z0  
 */
	private int mandelbrot(ComplexNumber z)
	{
		int count = 0;
		ComplexNumber z0 = z;
		ComplexNumber zn = new ComplexNumber(0,0);		
		double modulo = zn.mod();		
		
		while ((count<255) && ((modulo)<2)){ //Converge cuando el modulo es < 2					 
				zn = ComplexNumber.add ( zn.square(), z0); 	        	     	
				modulo = zn.mod();   	 		 			
				count++;		
		}	
		return (count);  		
	} 
	/**
	Fractal de spider: 	C= Z(0) ;  
	  					Zn+1 = Zn(2) + Cn ;
	  					Cn+1 = (Cn/2) + Zn+1  
	 */
	private int spider(ComplexNumber z)
	{
		int count = 0;
		ComplexNumber c = z;
		ComplexNumber zn = z;		
		double modulo = zn.mod();		
		
		while ((count<255) && ((modulo)<2)){ //Converge cuando el modulo es < 2					 
				zn = ComplexNumber.add ( zn.square(), c);	        	     	
				c = ComplexNumber.add (ComplexNumber.divide(c, new ComplexNumber(2,0)),zn);
				modulo = zn.mod();   	 		 			
				count++;		
		}	
		
		return (count);  /// Esta es la variable que vamos a usar para colorear		
	} 
	/**
	Fractal de Manowar:	C= Z(0) = z1(0) ;  
	  					Zn+1 = Zn(2) +Z1n + C ;
	  					Z1n+1 = Zn  
	 */
	private int manowar(ComplexNumber z)
	{
		int count = 0;
		ComplexNumber c = z;
		ComplexNumber zn = z;
		ComplexNumber zOneN = z;
		ComplexNumber znAux = z;// para tener el zn para actualizar z1n+1
		double modulo = zn.mod();		
		
		while ((count<255) && ((modulo)<2)){ //Converge cuando el modulo es < 2					 
			zn = ComplexNumber.add ( zn.square(), ComplexNumber.add(zOneN,c)); 	        	     	
			zOneN = znAux;
			znAux = zn;
			modulo = zn.mod();   	 		 			
			count++;		
		}	
		
		return (count);  /// Esta es la variable que vamos a usar para colorear		
	}
}

