import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class Ventana extends JFrame implements ActionListener {
	
	private JButton bPintar; private JButton bFractal;
	private JPanel lienzo;
	private Image img;
		
	public Ventana()
	{
		 super();            // usamos el contructor de la clase padre JFrame
		 configWindows();   // configuramos la ventana
	     initComponent();   // inicializamos los atributos o componentes
	}
	
	private void initComponent() 
	{
		
		// creamos el boton y lo configuramos
        bPintar = new JButton(); bPintar.setText("Degrade");  
        bPintar.setBounds(600, 0, 200, 30); bPintar.addActionListener(this);      
        this.add(bPintar);
        bFractal = new JButton(); bFractal.setText("Fractal");  
        bFractal.setBounds(600, 30, 200, 30); bFractal.addActionListener(this);      
        this.add(bFractal);
        //creamos el panel y lo configuramos
        lienzo = new Lienzo();
        lienzo.setVisible(true);
        this.add(lienzo);
        
        
        
        
    }		
	
	private void configWindows() 
	{
        this.setTitle("Jugando con los colores");                
        this.setSize(800, 600);                                 // colocamos tamanio a la ventana (ancho, alto)
        this.setLocationRelativeTo(null);                       // centramos la ventana en la pantalla
        this.setLayout(null);                                   // no usamos ningun layout, solo asi podremos dar posiciones a los componentes
        this.setResizable(false);                               // hacemos que la ventana no sea redimiensionable
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // hacemos que cuando se cierre la ventana termina todo proceso		
	}

	public void actionPerformed(ActionEvent evento) 
	{
		if (evento.getSource() == bPintar) 
			((Lienzo) lienzo).degrade();
		else
			((Lienzo) lienzo).map();
	}
	
}
